import React, { Component } from "react";
import DesktopMainInterface from "./desktopMainInterface";
import { isMobileOnly } from "react-device-detect";
import MobileMainInterface from "./mobile/mobileMainInterface";
import CheckLoginOnWebModal from "./checkLoginOnWebModal";
class MainInterface extends Component {
  state = {};
  render() {
    let mobileInterface = <MobileMainInterface data={this.props.data} />;
    let desktopInterface = <DesktopMainInterface data={this.props.data} />;
    if (isMobileOnly) {
      return mobileInterface;
    } else {
      return desktopInterface;
    }
  }
}

export default MainInterface;
