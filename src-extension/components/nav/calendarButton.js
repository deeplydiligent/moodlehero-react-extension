import React, { Component } from "react";
import { Link } from 'react-router-dom';
class CalendarButton extends Component {
  state = {};
  render() {
    return (
      <Link to="/home/calendar">
        <button
          title="Calendar"
          className="bg-transparent hover:bg-orange text-orange-dark font-semibold hover:text-white py-2 px-4 border border-orange hover:border-transparent rounded text-3xl ml-6"
        >
          <i className="fas fa-calendar" />
        </button>
      </Link>
    );
  }
}

export default CalendarButton;
