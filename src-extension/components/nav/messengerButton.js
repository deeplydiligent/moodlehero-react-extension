import React, { Component } from "react";
class MessengerButton extends Component {
  state = {};
  render() {
    return (
      <a target="_blank" href="https://uniboard.app/messenger">
        <button
          title="Messenger"
          className="bg-transparent hover:bg-teal text-teal-dark font-semibold hover:text-white py-2 px-4 border border-teal hover:border-transparent rounded text-3xl ml-6"
        >
          <i class="fas fa-comment-dots" />
        </button>
      </a>
    );
  }
}

export default MessengerButton;
